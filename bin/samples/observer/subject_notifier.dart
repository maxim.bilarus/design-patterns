import 'dart:async';
import 'dart:math';
import 'data.dart';
import 'helpers.dart';
import 'observers.dart';

/// Интерфейс для реализации субъекта-оповещателя.
abstract class SubjectNotifier {
  /// Регистрирует обсервера в субъекте.
  void registerObserver(Observer obs);

  /// Удаляет обсервера из субъекта.
  void removeObserver(Observer obs);

  /// Передает новые данные обсерверам.
  // ignore: unused_element
  void _notifyListeners();
}

/// Сенсор погоды, уведомляет дисплеи об изменениях погоды каждые 5 секунд.
class WeatherSensor implements SubjectNotifier {
  /// Обсерверы.
  final List<Observer> _obses = [];

  /// Данные о погоде за все время.
  final List<WeatherData> _weatherData = [];

  /// Крон.
  final Cron _cron = Cron();

  /// Добавляет данные о погоде.
  set currentData(WeatherData data) {
    _weatherData.add(data);
    _notifyListeners();
  }

  /// Уведомляет погодные дисплеи.
  @override
  void _notifyListeners() {
    _obses.forEach((obs) => obs.update(_weatherData));
  }

  /// Регистрирует погодный дисплей.
  @override
  void registerObserver(Observer obs) {
    if (!_obses.contains(obs)) {
      _obses.add(obs);
    }
  }

  /// Удаляет погодный дисплей.
  @override
  void removeObserver(Observer obs) {
    if (_obses.contains(obs)) {
      _obses.remove(obs);
    }
  }

  /// Активирует датчик.
  void activate() {
    _cron.performEvery(Duration(seconds: 5), getWeatherData);
  }

  /// Выключает датчик.
  void dispose() {
    _obses.clear();
    _weatherData.clear();
    _cron.dispose();
  }

  /// Считывает текущие данные о погоде.
  void getWeatherData() async {
    await Future.delayed(Duration(seconds: 1));
    currentData = WeatherData(
      temperature: Random().nextInt(40) - 15,
      humidity: Random().nextInt(30) + 30,
      pressure: Random().nextInt(10) + 750,
    );
  }
}

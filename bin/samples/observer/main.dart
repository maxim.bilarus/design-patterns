import 'observers.dart';
import 'subject_notifier.dart';

void main() async {
  // Создаем наблюдателей субъекта, в данном случае датчика погоды.
  final weahterStatisticsDisplayer = WeatherStatisticDisplayer();
  final currentWeatherDisplayer = CurrentWeatherDisplayer();
  final forecastWeatherDisplayer = WeatherForecastDisplayer();

  // Создаем субъект-уведомитель, который получает данные о погоде и уведомляет наблюдателей.
  final weatherSensor = WeatherSensor();

  // Регистрируем дисплеи отображения погодных данных, как наблюдателей.
  weatherSensor
    ..registerObserver(currentWeatherDisplayer)
    ..registerObserver(weahterStatisticsDisplayer)
    ..registerObserver(forecastWeatherDisplayer);

  // Активируем датчик.
  weatherSensor.activate();

  await Future.delayed(Duration(seconds: 10));

  // Убираем подписку дисплея текущей погоды.
  weatherSensor.removeObserver(currentWeatherDisplayer);

  await Future.delayed(Duration(seconds: 5));

  // Убираем подписку на прогноз ближайшей погоды.
  weatherSensor.removeObserver(forecastWeatherDisplayer);

  await Future.delayed(Duration(seconds: 10));

  // Деактивируем датчик, стираем данные.
  weatherSensor.dispose();
}

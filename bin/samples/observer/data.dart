/// Данные о погоде.
class WeatherData {
  /// Температура в градусах Цельсия.
  int temperature;

  /// Влажность в %.
  int humidity;

  /// Давление в мм. рт. столба.
  int pressure;
  WeatherData({this.temperature, this.humidity, this.pressure});
}

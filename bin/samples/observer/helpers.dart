import 'typedefs.dart';

/// Выполняет определенное действие каждые n секунд.
class Cron {
  /// Активирован ли крон.
  bool isActivated = true;

  /// Запускает крон.
  Future<void> performEvery(Duration duration, Callback f) async {
    if (isActivated) {
      await Future.delayed(duration);
      if (isActivated) {
        f();
        await performEvery(duration, f);
      }
    }
  }

  /// Закрывает крон.
  void dispose() {
    isActivated = false;
  }
}

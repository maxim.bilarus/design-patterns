import 'data.dart';

/// Обсервер, реализует метод update().
abstract class Observer<T> {
  /// Изменяет обсервера, при подписке на субъекта-оповещателя.
  void update(T data);
}

/// Отображает текущую погоду.
class CurrentWeatherDisplayer extends Observer<List<WeatherData>> {
  @override
  void update(List<WeatherData> data) {
    var weatherData = data.last;
    print(
        'Текущая погода:\nТемпература: ${weatherData.temperature} градусов Цельсия.\nВлажность ${weatherData.humidity} %.\nДавление: ${weatherData.pressure} мм. рт. столба.');
  }
}

/// Отображает статистику погоды за все время.
class WeatherStatisticDisplayer extends Observer<List<WeatherData>> {
  @override
  void update(List<WeatherData> data) {
    var averageData = data.reduce(
      (a, b) => WeatherData(
        humidity: ((a.humidity + b.humidity) / 2).round(),
        temperature: ((a.temperature + b.temperature) / 2).round(),
        pressure: ((a.pressure + b.pressure) / 2).round(),
      ),
    );

    print(
        'Статистика погоды:\nТемпература: ${averageData.temperature} градусов Цельсия.\nВлажность ${averageData.humidity} %.\nДавление: ${averageData.pressure} мм. рт. столба.');
  }
}

/// Отображает прогноз погоды на ближайшее время.
class WeatherForecastDisplayer extends Observer<List<WeatherData>> {
  @override
  void update(List<WeatherData> data) {
    if (data.last.temperature > 0 && data.last.humidity >= 55) {
      print('Дождливо.');
    } else if (data.last.humidity >= 30 && data.last.humidity < 45) {
      print('Солнечно.');
    } else if (data.last.humidity >= 45 && data.last.humidity < 55) {
      print('Облачно.');
    } else if (data.last.temperature <= 0 && data.last.humidity >= 55) {
      print('Снег.');
    }
  }
}

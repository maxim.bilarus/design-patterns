import 'decorate_subjects.dart';

/// Декоратор добавки, имеет тот же тип, что и напиток,
/// нужен чтобы влиять на поведение напитка.
abstract class ConditmentDecorator extends Beverage {}

class Mocha extends ConditmentDecorator {
  Beverage beverage;
  Mocha(this.beverage);

  @override
  String receiveDescription() => beverage.receiveDescription() + ', Mocha';

  @override
  double receiveCost() => 0.20 + beverage.receiveCost();
}

class Milk extends ConditmentDecorator {
  Beverage beverage;
  Milk(this.beverage);

  @override
  String receiveDescription() => beverage.receiveDescription() + ', Milk';

  @override
  double receiveCost() => 0.1 + beverage.receiveCost();
}

class Whip extends ConditmentDecorator {
  Beverage beverage;
  Whip(this.beverage);

  @override
  String receiveDescription() => beverage.receiveDescription() + ', Whip';

  @override
  double receiveCost() => 0.5 + beverage.receiveCost();
}

class Soy extends ConditmentDecorator {
  Beverage beverage;
  Soy(this.beverage);

  @override
  String receiveDescription() => beverage.receiveDescription() + ', Soy';

  @override
  double receiveCost() => 0.18 + beverage.receiveCost();
}

/// Напиток.
abstract class Beverage {
  String receiveDescription();

  double receiveCost();
}

class Expresso extends Beverage {
  @override
  String receiveDescription() => 'Expresso';

  @override
  double receiveCost() => 1.99;
}

class HouseBlend extends Beverage {
  @override
  String receiveDescription() => 'House Blend Coffee';

  @override
  double receiveCost() => 0.89;
}

class DarkRoast extends Beverage {
  @override
  String receiveDescription() => 'Dark Roast Coffee';

  @override
  double receiveCost() => 2.44;
}

class Decaf extends Beverage {
  @override
  String receiveDescription() => 'Decaf coffee';

  @override
  double receiveCost() => 1.63;
}

import 'decorate_subjects.dart';
import 'decorators.dart';

void main() {
  // Собираем кофе.
  var houseBlendSoy = Soy(HouseBlend());
  var expressoMochaMilk = Milk(Mocha(Expresso()));
  var darkRoastMilkDoubleWhip = Whip(Whip(Milk(DarkRoast())));
  var decafMilkDoubleSoyDoubleMochaWhip =
      Whip(Mocha(Mocha(Soy(Soy(Milk(Decaf()))))));

  // Выводим цены и описание.
  print('''
  ${houseBlendSoy.receiveDescription()} ${houseBlendSoy.receiveCost()}
  ${expressoMochaMilk.receiveDescription()} ${expressoMochaMilk.receiveCost()}
  ${darkRoastMilkDoubleWhip.receiveDescription()} ${darkRoastMilkDoubleWhip.receiveCost()}
  ${decafMilkDoubleSoyDoubleMochaWhip.receiveDescription()} ${decafMilkDoubleSoyDoubleMochaWhip.receiveCost()}
 ''');
}

import '../factory.dart';
import 'pizza.dart';

class NYPizzaStore extends PizzaStore {
  @override
  Pizza createPizza(String item) {
    Pizza pizza;
    PizzaIngredientFactory ingredientFactory = NYPizzaIngredientFactory();
    if (item == 'cheese') {
      pizza = CheesePizza(ingredientFactory);
      pizza.setName('New York Style Cheese Pizza');
    } else if (item == 'veggie') {
      pizza = VeggiePizza(ingredientFactory);
      pizza.setName('New York Style Veggie Pizza');
    } else if (item == 'clam') {
      pizza = ClamPizza(ingredientFactory);
      pizza.setName('New York Style Clam Pizza');
    } else if (item == 'pepperoni') {
      pizza = PepperoniPizza(ingredientFactory);
      pizza.setName('New York Style Pepperoni Pizza');
    }
    return pizza;
  }
}

class ChicagoPizzaStore extends PizzaStore {
  @override
  Pizza createPizza(String item) {
    Pizza pizza;
    PizzaIngredientFactory ingredientFactory = ChicagoPizzaIngredientFactory();
    if (item == 'cheese') {
      pizza = CheesePizza(ingredientFactory);
      pizza.setName('Chicago Style Cheese Pizza');
    } else if (item == 'veggie') {
      pizza = VeggiePizza(ingredientFactory);
      pizza.setName('Chicago Style Veggie Pizza');
    } else if (item == 'clam') {
      pizza = ClamPizza(ingredientFactory);
      pizza.setName('Chicago Style Clam Pizza');
    } else if (item == 'pepperoni') {
      pizza = PepperoniPizza(ingredientFactory);
      pizza.setName('Chicago Style Pepperoni Pizza');
    }
    return pizza;
  }
}

abstract class PizzaStore {
  Pizza orderPizza(String type) {
    Pizza pizza;
    pizza = createPizza(type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }

  Pizza createPizza(String type);
}

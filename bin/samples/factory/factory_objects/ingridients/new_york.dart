import 'base.dart';

class ThinCrustDough extends Dough {
  @override
  String toString() => 'Thin Crust Dough';
}

class MarinaraSauce extends Sauce {
  @override
  String toString() => 'Marinara Sauce';
}

class ReggianoCheese extends Cheese {
  @override
  String toString() => 'Reggiano Cheese';
}

class Garlic extends Veggie {
  @override
  String toString() => 'Garlic';
}

class Onion extends Veggie {
  @override
  String toString() => 'Onion';
}

class Mushroom extends Veggie {
  @override
  String toString() => 'Mushroom';
}

class RedPepper extends Veggie {
  @override
  String toString() => 'Red Pepper';
}

class SlicedPepperoni extends Pepperoni {
  @override
  String toString() => 'Sliced Pepperoni';
}

class FreshClams extends Clams {
  @override
  String toString() => 'Fresh Clams';
}

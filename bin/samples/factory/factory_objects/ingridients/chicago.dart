import 'base.dart';

class BlackOlives extends Veggie {
  @override
  String toString() => 'Black Olives';
}

class Spinach extends Veggie {
  @override
  String toString() => 'Spinach';
}

class SlicedChicagoPepperoni extends Pepperoni {
  @override
  String toString() => 'Sliced Chicago Pepperoni';
}

class FrozenClams extends Clams {
  @override
  String toString() => 'Frozen Clams';
}

class MozzarellaChesse extends Cheese {
  @override
  String toString() => 'Mozzarella Chesse';
}

class PlumTomatoSauce extends Sauce {
  @override
  String toString() => 'Plum Tomato Sauce';
}

class ThickCrustDough extends Dough {
  @override
  String toString() => 'Thick Crust Dough';
}

class EggPlant extends Veggie {
  @override
  String toString() => 'Egg Plant';
}

import '../factory.dart';
import 'ingridients/base.dart';

abstract class Pizza {
  String name;
  Dough dough;
  Sauce sauce;
  List<Veggie> veggies;
  Cheese cheese;
  Pepperoni pepperoni;
  Clams clam;

  void prepare();

  void bake() {
    print('Bake for 25 minutes at 350');
  }

  void cut() {
    print('Cutting the pizza into diagonal slices');
  }

  void box() {
    print('Place pizza in official PizzaStore box');
  }

  void setName(String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }

  String get ingredients =>
      ([dough, sauce, ...?veggies, cheese, pepperoni, clam]
            ..removeWhere((ingredient) => ingredient == null))
          .toList()
          .join(', ') +
      '.';
}

class CheesePizza extends Pizza {
  PizzaIngredientFactory ingredientFactory;
  CheesePizza(PizzaIngredientFactory ingredientFactory) {
    this.ingredientFactory = ingredientFactory;
  }

  @override
  void prepare() {
    print('Preparing ' + name);
    dough = ingredientFactory.createDough();
    sauce = ingredientFactory.createSauce();
    cheese = ingredientFactory.createCheese();
  }
}

class ClamPizza extends Pizza {
  PizzaIngredientFactory ingredientFactory;
  ClamPizza(PizzaIngredientFactory ingredientFactory) {
    this.ingredientFactory = ingredientFactory;
  }

  @override
  void prepare() {
    print('Preparing ' + name);
    dough = ingredientFactory.createDough();
    sauce = ingredientFactory.createSauce();
    cheese = ingredientFactory.createCheese();
    clam = ingredientFactory.createClam();
  }
}

class PepperoniPizza extends Pizza {
  PizzaIngredientFactory ingredientFactory;
  PepperoniPizza(PizzaIngredientFactory ingredientFactory) {
    this.ingredientFactory = ingredientFactory;
  }

  @override
  void prepare() {
    print('Preparing ' + name);
    dough = ingredientFactory.createDough();
    sauce = ingredientFactory.createSauce();
    cheese = ingredientFactory.createCheese();
    pepperoni = ingredientFactory.createPepperoni();
  }
}

class VeggiePizza extends Pizza {
  PizzaIngredientFactory ingredientFactory;
  VeggiePizza(PizzaIngredientFactory ingredientFactory) {
    this.ingredientFactory = ingredientFactory;
  }

  @override
  void prepare() {
    print('Preparing ' + name);
    dough = ingredientFactory.createDough();
    sauce = ingredientFactory.createSauce();
    cheese = ingredientFactory.createCheese();
    veggies = ingredientFactory.createVeggies();
  }
}

import 'factory_objects/store.dart';

void main() {
  // Создаем магазин пиццы NY и Chicago.
  PizzaStore nyPizzaStore = NYPizzaStore();
  PizzaStore chicagoPizzaStore = ChicagoPizzaStore();
  // Заказываем вегетерианскую нью-йоркскую пиццу.
  var nyPizza = nyPizzaStore.orderPizza('veggie');
  print(nyPizza.ingredients);
  print('\n\n');
  // Заказываем чикагскую пиццу с моллюсками.
  var chicagoPepperoniPizza = chicagoPizzaStore.orderPizza('clam');
  print(chicagoPepperoniPizza.ingredients);
}

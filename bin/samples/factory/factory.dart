import 'factory_objects/ingridients/base.dart';
import 'factory_objects/ingridients/chicago.dart';
import 'factory_objects/ingridients/new_york.dart';

abstract class PizzaIngredientFactory {
  Dough createDough();
  Sauce createSauce();
  Cheese createCheese();
  List<Veggie> createVeggies();
  Pepperoni createPepperoni();
  Clams createClam();
}

class NYPizzaIngredientFactory implements PizzaIngredientFactory {
  @override
  Dough createDough() {
    return ThinCrustDough();
  }

  @override
  Sauce createSauce() {
    return MarinaraSauce();
  }

  @override
  Cheese createCheese() {
    return ReggianoCheese();
  }

  @override
  List<Veggie> createVeggies() {
    var veggies = [Garlic(), Onion(), Mushroom(), RedPepper()];
    return veggies;
  }

  @override
  Pepperoni createPepperoni() {
    return SlicedPepperoni();
  }

  @override
  Clams createClam() {
    return FreshClams();
  }
}

class ChicagoPizzaIngredientFactory extends PizzaIngredientFactory {
  @override
  Dough createDough() {
    return ThickCrustDough();
  }

  @override
  Sauce createSauce() {
    return PlumTomatoSauce();
  }

  @override
  Cheese createCheese() {
    return MozzarellaChesse();
  }

  @override
  List<Veggie> createVeggies() {
    var veggies = [BlackOlives(), Spinach(), EggPlant()];
    return veggies;
  }

  @override
  Pepperoni createPepperoni() {
    return SlicedChicagoPepperoni();
  }

  @override
  Clams createClam() {
    return FrozenClams();
  }
}
